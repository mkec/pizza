import React, { Component } from 'react';
import { connect } from 'react-redux'
import { BrowserRouter, Route, Switch /* HashRouter */} from 'react-router-dom'
import Navbar from './components/Navbar/Navbar'
import Logout from './containers/Auth/Logout';
import Auth from './containers/Auth/Auth';
import Home from './components/Home/Home';
import Cart from './components/Cart/Cart';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <Switch>
            <Route path="/login" component={Auth} />
            <Route path="/logout" component={Logout} />
            <Route path="/cart" component={Cart} />
            <Route exact path="/" component={Home} />
          </Switch>
        </div>
      </BrowserRouter>

    );
  }
}
const mapStateToProps = (state) => {
    return {
        api_token: state.ardcr.token
    }
}
/* const mapDispatchToProps = (dispatch) => {

    return {
      
    }
} */

export default connect(mapStateToProps)(App)