import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner';
import { Link } from 'react-router-dom'
import Input from '../UI/Form/Input/Input';
import History from '../History/History';
import * as actions from '../../store/actions/actions';
import * as actionTypes from '../../store/actions/actionTypes';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import classes from '../Home/Home.module.css';
import cClasses from './Cart.module.css';
import M from "materialize-css";
/* import Recipe from './Recipe' */
class Cart extends Component {
    constructor(props) {
        super(props);
        let currency = localStorage.getItem('cur') ? localStorage.getItem('cur') : 'EUR';
        let locale = localStorage.getItem('locale') ? localStorage.getItem('locale') : 'de-DE';
        let curconv = currency === 'EUR' ? 1 : process.env.REACT_APP_CONV;
        this.state = {
            locale,
            currency,
            curconv,
            cartItems: []
        }
        if (!localStorage.getItem('cur')) localStorage.setItem('cur', currency);
        if (!localStorage.getItem('locale')) localStorage.setItem('locale', locale);
        props.initializeLoader();
        props.onCartDataGet(props.api_token, this.props.activeCartId);
    }
    //to remove the item completely
    handleRemove = (id) => {
        this.props.removeItem(id, this.props.api_token, this.props.activeCartId);
    }
    //to add the quantity
    handleAddQuantity = (id, i, size) => {
        let items = {
            ...this.state.cartItems
        }
        let item = {
            ...items[i]
        }

        let S = (size === 'S') ? this.state.cartItems[i].quantity.S + 1 : this.state.cartItems[i].quantity.S;
        let M = (size === 'M') ? this.state.cartItems[i].quantity.M + 1 : this.state.cartItems[i].quantity.M;
        let L = (size === 'L') ? this.state.cartItems[i].quantity.L + 1 : this.state.cartItems[i].quantity.L;

        item.quantity = { S, M, L }
        items[i] = item;

        this.setState({ cartItems: items });
        this.props.initializeLoader();
        this.props.addToCart({ id: items[i].id, img:items[i].img, title: items[i].title, prices:items[i].prices, quantity: items[i].quantity, api_token: this.props.api_token, cart_id: this.props.activeCartId });
    }
    //to substruct from the quantity
    handleSubtractQuantity = (id, i, size) => {
        let items = {
            ...this.state.cartItems
        }
        let item = {
            ...items[i]
        }
        let S = (size === 'S' && this.state.cartItems[i].quantity.S > 0) ? this.state.cartItems[i].quantity.S - 1 : this.state.cartItems[i].quantity.S;
        let M = (size === 'M' && this.state.cartItems[i].quantity.M > 0) ? this.state.cartItems[i].quantity.M - 1 : this.state.cartItems[i].quantity.M;
        let L = (size === 'L' && this.state.cartItems[i].quantity.L > 0) ? this.state.cartItems[i].quantity.L - 1 : this.state.cartItems[i].quantity.L;

        item.quantity = { S, M, L }
        items[i] = item;

        this.setState({ cartItems: items });
        this.props.initializeLoader();
        this.props.addToCart({ id: items[i].id, img:items[i].img, title: items[i].title, prices:items[i].prices, quantity: items[i].quantity, api_token: this.props.api_token, cart_id: this.props.activeCartId });
    }

    static getDerivedStateFromProps(props, state) {

        if (JSON.stringify(props.items) !== JSON.stringify(state.cartItems)) {
            return {
                cartItems: props.items
            }
        }
        return { ...state }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (JSON.stringify(this.props.items) !== JSON.stringify(nextProps.items)) {
            return true;
        }
        if (JSON.stringify(this.props.orders) !== JSON.stringify(nextProps.orders)) {
            return true;
        }
        if (this.props.deliveryForm !== nextProps.deliveryForm) {
            return true;
        }
        if (this.state.currency !== nextState.currency) {
            return true;
        }
        return false;
    }

    handleChange = event => {
        let curconv = event.target.options[event.target.selectedIndex].text === 'EUR' ? 1 : process.env.REACT_APP_CONV;
        this.setState(
            {
                currency: event.target.options[event.target.selectedIndex].text,
                locale: event.target.value,
                curconv
            }
        );
        localStorage.setItem('cur', event.target.options[event.target.selectedIndex].text);
        localStorage.setItem('locale', event.target.value);
    }

    handleSubmit = event => {
        let memoizedFn = this.memoize(this.calculateSum);

        const total_pizzas_price = memoizedFn(this.props.items, this.state.curconv);
        const currency = this.state.currency;
        const shipping_price = 6.00;
        const total_price = total_pizzas_price + shipping_price;
        const token = this.props.api_token ? this.props.api_token : null;
        const contact_details = localStorage.getItem('deliveryAddress') ? localStorage.getItem('deliveryAddress') : null;

        const order = {
            cart_id:this.props.activeCartId,
            total_pizzas_price,
            currency,
            shipping_price,
            total_price,
            token,
            contact_details,
            user_id:1 //overriden - we should send token and check for user for sent token - but we could not get history without login...now we can bc user will be always set
        }
        this.props.initializeLoader();
        this.props.onSubmitOrder(order);
        /* this.props.onCartDataGet(this.props.api_token, this.props.activeCartId); */
        /* this.props.onOrdersGet(this.props.api_token, null); */
    }

    componentDidMount() {
        // Auto initialize all the things!
        M.AutoInit();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.ordered !== this.props.ordered) { 
            this.props.onOrdersGet(this.props.api_token, null);
        }
    }

    calculateSum = (items, conv) => {
        let sumTotal = 0;
        if (typeof items === 'undefined') return sumTotal;
        items.forEach(element => {
            sumTotal += (element.quantity.S * element.prices.S * conv) + (element.quantity.M * element.prices.M * conv) + (element.quantity.L * element.prices.L * conv);
        });
        return sumTotal;
    }

    memoize = fn => {
        let cache = {};
        return (...args) => {
            let stringifiedArgs = JSON.stringify(args[0], ['id', 'quantity', 'S', 'M', 'L', 'prices']) + args[1];
            let result = cache[stringifiedArgs] = cache[stringifiedArgs] || fn(...args);
            return result;
        };
    };

    render() {
        let memoizedFn = this.memoize(this.calculateSum);

        const formElementsArray = [];
        for (let key in this.props.deliveryForm) {
            formElementsArray.push({
                id: key,
                config: this.props.deliveryForm[key]
            });
        }
        let tBtn = (
            <button className="waves-effect waves-light btn pink remove formbutton" onClick={(e) => { this.handleSubmit(e) }} disabled={!this.props.formIsValid}>Order now</button>
        );
        let tForm = (
            <form className={cClasses.form}>
                {formElementsArray.map(formElement => {
                    return <Input
                        key={formElement.id}
                        elementConfig={formElement.config.elementConfig}
                        title={formElement.config.elementConfig.title}
                        placeholder={formElement.config.elementConfig.placeholder}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.props.onInputChanged(event, formElement.id)} />
                })}
            </form>
        );

        let addedItems = (typeof this.props.items !== 'undefined' && this.props.items.length > 0) ?
            (
                this.state.cartItems.map((item, i) => {
                    try {
                        item.prices = JSON.parse(item.prices);
                    } catch (e) {
                        item.prices = item.prices;//under comment - just lets have something on fallback :D
                    }
                    try {
                        item.quantity = JSON.parse(item.quantity);
                    } catch (e) {
                        item.quantity = item.quantity;//under comment - just lets have something on fallback :D
                    }
                    return (

                        <li className="collection-item avatar" key={item.id}>
                            <div className="item-img">
                                <img src={item.img} alt={item.img} className="" />
                            </div>

                            <div className="item-desc">
                                <span className="title">{item.title}</span>
                                <p>{item.desc}</p>

                                <p>
                                    <span className={[classes.circle, "red", classes.pcs].join(' ')}>S</span> {item.quantity.S}
                                    <span className={[classes.circle, "red", classes.pcs, classes.marginLeft].join(' ')}>M</span> {item.quantity.M}
                                    <span className={[classes.circle, "red", classes.pcs, classes.marginLeft].join(' ')}>L</span> {item.quantity.L}
                                </p>

                                <div className="add-remove">
                                    <Link to="/cart"><i className="material-icons" style={{ fontSize: '24px', marginLeft: '-5px' }} onClick={() => { this.handleAddQuantity(item.id, i, 'S') }}>arrow_drop_up</i></Link>
                                    <Link to="/cart"><i className="material-icons" style={{ fontSize: '24px' }} onClick={() => { this.handleSubtractQuantity(item.id, i, 'S') }}>arrow_drop_down</i></Link>

                                    <Link to="/cart"><i className="material-icons" style={{ fontSize: '24px', marginLeft: '5px' }} onClick={() => { this.handleAddQuantity(item.id, i, 'M') }}>arrow_drop_up</i></Link>
                                    <Link to="/cart"><i className="material-icons" style={{ fontSize: '24px' }} onClick={() => { this.handleSubtractQuantity(item.id, i, 'M') }}>arrow_drop_down</i></Link>

                                    <Link to="/cart"><i className="material-icons" style={{ fontSize: '24px', marginLeft: '5px' }} onClick={() => { this.handleAddQuantity(item.id, i, 'L') }}>arrow_drop_up</i></Link>
                                    <Link to="/cart"><i className="material-icons" style={{ fontSize: '24px' }} onClick={() => { this.handleSubtractQuantity(item.id, i, 'L') }}>arrow_drop_down</i></Link>
                                </div>

                                <span className={[cClasses.price, cClasses.sprice].join(' ')}>{new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(item.prices.S * item.quantity.S * this.state.curconv)} + {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(item.prices.M * item.quantity.M * this.state.curconv)} + {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(item.prices.L * item.quantity.L * this.state.curconv)}</span>
                                <span className={cClasses.price}><b>Total</b>: {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(((item.prices.S * item.quantity.S) + (item.prices.M * item.quantity.M) + (item.prices.L * item.quantity.L)) * this.state.curconv)}</span>
                                <button className="waves-effect waves-light btn pink remove" onClick={() => { this.handleRemove(item.id) }}>Remove</button>
                            </div>

                        </li>

                    )
                })
            ) :

            (
                <p>Nothing.</p>
            )
        return (
            <div className="container">
                <Loader type="Rings" color="#00BFFF" height={80} width={80} visible={this.props.loaderLoading && this.props.numOfAsync > 0} style={{position: 'fixed',
                    top: '0',
                    right: '0',
                    bottom: '0',
                    left: '0',
                    paddingTop:'20%',
                    textAlign:'center',
                    background: 'rgba(255, 255, 255, 0.4)',
                    zIndex: '9999'
                }}/>
                <div style={{ float: 'right', width: '100%' }}>
                    <div style={{ width: '70px', float: 'right' }}>
                        <select
                            onChange={this.handleChange}
                            defaultValue={this.state.locale}
                        >
                            <option value="en-US">USD</option>
                            <option value="de-DE">EUR</option>
                        </select>
                    </div>
                </div>
                <div className="cart">
                    <h5>You have ordered:</h5>
                    <ul className="collection">
                        {addedItems}
                    </ul>
                    <ul className=" collection" style={{ padding: '10px 20px' }}>
                        <li className={cClasses.moneyAlign}><span className={cClasses.label}>Total Pizza:</span><span className={cClasses.money}> {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(memoizedFn(this.props.items, this.state.curconv))}</span></li>
                        <li className={cClasses.moneyAlign}><span className={cClasses.label}>Shipping Costs:</span><span className={cClasses.money}> {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(6 * this.state.curconv)}</span></li>
                        <li className={cClasses.moneyAlign}><span className={cClasses.label}>Total Order:</span><span className={cClasses.money}> {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(memoizedFn(this.props.items, this.state.curconv) + (6 * this.state.curconv))}</span></li>
                    </ul>
                    <h5>Contact details:</h5>
                    <ul className="collection" style={{ padding: '0 20px' }}>
                        <li className="collection-item avatar">
                            {tForm}
                        </li>
                    </ul>
                    {tBtn}
                </div>
                <History currency={this.state.currency} />
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        items: state.rdcr.cart,
        orders: state.rdcr.orders,
        api_token: state.ardcr.token,
        deliveryForm: state.rdcr.deliveryForm,
        formIsValid: state.rdcr.formIsValid,
        activeCartId: state.rdcr.activeCartId,
        ordered: state.rdcr.ordered,
        loaderLoading: state.rdcr.loaderLoading,
        numOfAsync:state.rdcr.numOfAsync
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        removeItem: (id, token, activeCartId) => { dispatch(actions.removeItem(id, token, activeCartId)) },
        addToCart: (obj) => dispatch(actions.addToCart(obj)),
        onDataGet: (pp, cp) => dispatch(actions.onDataGet(pp, cp)),
        onCartDataGet: (token, activeCartId) => dispatch(actions.onCartDataGet(token, activeCartId)),
        onInputChanged: (event, formId) => dispatch({ type: actionTypes.INPUT_CHANGE, value: event.target.value, formId: formId }),
        onSubmitOrder: (obj) => dispatch(actions.onSubmitOrder(obj)),
        onOrdersGet: (token, identifier) => dispatch(actions.onOrdersGet(token, identifier)),
        initializeLoader: () => dispatch({ type: actionTypes.INITIALIZE_LOADER }),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Cart)