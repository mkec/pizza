import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/actions';
/* import Recipe from './Recipe' */
class Cart extends Component {
    constructor(props) {
        super(props);
        let currency = localStorage.getItem('cur') ? localStorage.getItem('cur') : 'EUR';
        let locale = localStorage.getItem('locale') ? localStorage.getItem('locale') : 'de-DE';
        let curconv = currency === 'EUR' ? 1 : process.env.REACT_APP_CONV;
        this.state = {
            locale,
            currency,
            curconv,
            orders: []
        }
        if (!localStorage.getItem('cur')) localStorage.setItem('cur', currency);
        if (!localStorage.getItem('locale')) localStorage.setItem('locale', locale);

        this.props.onOrdersGet(this.props.api_token, null);
    }

    static getDerivedStateFromProps(props, state) {

        if (JSON.stringify(props.orders) !== JSON.stringify(state.orders)) {
            return {
                orders: props.orders
            }
        }
        if (props.currency !== state.currency) {
            let currency = props.currency;
            let locale = (currency === 'EUR') ? 'de-DE' : 'en-US';
            let curconv = currency === 'EUR' ? 1 : process.env.REACT_APP_CONV;
            return {
                locale,
                currency,
                curconv
            }
        }
        return { ...state }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (JSON.stringify(this.props.orders) !== JSON.stringify(nextProps.orders)) {
            return true;
        }
        if (this.state.currency !== nextState.currency) {
            return true;
        }
        if (this.props.currency !== nextProps.currency) {
            return true;
        }
        return false;
    }

    /* handleChange = event => {
        let curconv = event.target.options[event.target.selectedIndex].text === 'EUR' ? 1 : process.env.REACT_APP_CONV;
        this.setState(
            {
                currency: event.target.options[event.target.selectedIndex].text,
                locale: event.target.value,
                curconv
            }
        );
        localStorage.setItem('cur', event.target.options[event.target.selectedIndex].text);
        localStorage.setItem('locale', event.target.value);
    }

    componentDidMount() {
        // Auto initialize all the things!
        M.AutoInit();
    } */

    render() {
        let orderedItems = (typeof this.props.orders !== 'undefined' && this.props.orders.length > 0) ?
            (
                this.state.orders.map((item, i) => {
                    return (
                        <li key={i} className="collection-item avatar" style={{ padding: '5px 0 5px 20px' }}>
                            <div className="item-desc">
                                <p>Total: {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(item.total)}</p>
                                <p>Ordered: {item.created_at}</p>
                            </div>
                        </li>
                    )
                })
            ) :

            (
                <p>Nothing.</p>
            )
        return (
            <div className="cart">
                <h5>Order history:</h5>
                <ul className="collection">
                    {orderedItems}
                </ul>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        api_token: state.ardcr.token,
        orders: state.rdcr.orders
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onOrdersGet: (token, identifier) => { dispatch(actions.onOrdersGet(token, identifier)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Cart)