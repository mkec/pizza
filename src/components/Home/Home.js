import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner';
import * as actions from '../../store/actions/actions';
import * as actionTypes from '../../store/actions/actionTypes';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import classes from './Home.module.css';
import M from "materialize-css";

class Home extends Component {

    constructor(props) {
        super(props);
        let currency = localStorage.getItem('cur') ? localStorage.getItem('cur') : 'EUR';
        let locale = localStorage.getItem('locale') ? localStorage.getItem('locale') : 'de-DE';
        let curconv = currency === 'EUR' ? 1 : process.env.REACT_APP_CONV;
        this.state = {
            locale,
            currency,
            curconv,
            items: []
        }
        if (!localStorage.getItem('cur')) localStorage.setItem('cur', currency);
        if (!localStorage.getItem('locale')) localStorage.setItem('locale', locale);
        props.initializeLoader();
        props.onDataGet(props.api_token);
    }

    static getDerivedStateFromProps(props, state) {
        let itms = [];
        if (state.items.length !== 0) return { ...state }
        props.items.forEach(element => {
            itms.push(
                {
                    id: element.id,
                    img: element.img,
                    prices: element.prices,
                    title: element.title,
                    cart_id: element.cart_id,
                    quantity: {
                        S: 0,
                        M: 0,
                        L: 0
                    }
                }
            );
        });

        return {
            items: itms
        }
    }

    handleClick = (id, i, size) => {

        if ((this.props.cartInitialized === false && this.props.activeCartId === null) || (this.props.activeCartId !== null)) {
            let items = {
                ...this.state.items
            }
            let item = {
                ...items[i]
            }

            let S = (size === 'S') ? this.state.items[i].quantity.S + 1 : this.state.items[i].quantity.S;
            let M = (size === 'M') ? this.state.items[i].quantity.M + 1 : this.state.items[i].quantity.M;
            let L = (size === 'L') ? this.state.items[i].quantity.L + 1 : this.state.items[i].quantity.L;

            item.quantity = { S, M, L }
            items[i] = item;

            this.setState({ items });

            this.props.initializeLoader();
            this.props.initializeCart();
            this.props.addToCart({ id: items[i].id, img: items[i].img, title: items[i].title, prices: items[i].prices, quantity: items[i].quantity, api_token: this.props.api_token, cart_id: this.props.activeCartId });
        } else {
            console.log('Please wait...');
        }
        /* this.props.onCartDataGet(this.props.api_token, this.props.activeCartId); */
    }

    handleChange = event => {
        let curconv = event.target.options[event.target.selectedIndex].text === 'EUR' ? 1 : process.env.REACT_APP_CONV;
        this.setState(
            {
                currency: event.target.options[event.target.selectedIndex].text,
                locale: event.target.value,
                curconv
            }
        );
        localStorage.setItem('cur', event.target.options[event.target.selectedIndex].text);
        localStorage.setItem('locale', event.target.value);
    }

    componentDidMount() {
        // Auto initialize all the things!
        M.AutoInit();
    }

    render() {
        /* let deliveryForm = (

        ); */
        let itemList = this.props.items.map((item, i) => {
            try {
                item.prices = JSON.parse(item.prices);
            } catch (e) {
                // You can read e for more info
                // Let's assume the error is that we already have parsed the payload
                // So just return that
                item.prices = item.prices;//under comment - just lets have something on fallback :D
            }

            return (
                <div className="card" key={item.id}>
                    <div className="card-image">
                        <img src={item.img} alt={item.title} />
                        <span className="card-title">{item.title}</span>
                        <span to="/" className="btn-floating halfway-fab waves-light red" style={{ right: '136px' }} onClick={() => { this.handleClick(item.id, i, 'S') }}><i className="material-icons">add</i></span>
                        <span to="/" className="btn-floating halfway-fab waves-light red" style={{ right: '80px' }} onClick={() => { this.handleClick(item.id, i, 'M') }}><i className="material-icons">add</i></span>
                        <span to="/" className="btn-floating halfway-fab waves-light red" onClick={() => { this.handleClick(item.id, i, 'L') }}><i className="material-icons">add</i></span>
                    </div>

                    <div className="card-content">
                        <p className={classes.rightAlign}><span className={[classes.circle, "red", classes.pcs, classes.marginLeft].join(' ')}>S</span> {this.state.items[i].quantity.S} <span className={[classes.circle, "red", classes.pcs, classes.marginLeft].join(' ')}>M</span> {this.state.items[i].quantity.M} <span className={[classes.circle, "red", classes.pcs, classes.marginLeft].join(' ')}>L</span> {this.state.items[i].quantity.L}</p>
                        <p>{item.description.substring(0, 100)}...</p>
                        <p><span className={classes.circle}>S</span> {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(item.prices.S * this.state.curconv)} <span className={classes.circle}>M</span> {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(item.prices.M * this.state.curconv)} <span className={classes.circle}>L</span> {new Intl.NumberFormat(this.state.locale, { style: 'currency', currency: this.state.currency }).format(item.prices.L * this.state.curconv)}</p>
                    </div>
                </div>

            )
        })
        
        return (
            <div className="container">
                <Loader type="Rings" color="#00BFFF" height={80} width={80} visible={this.props.loaderLoading && this.props.numOfAsync > 0} style={{position: 'fixed',
                    top: '0',
                    right: '0',
                    bottom: '0',
                    left: '0',
                    paddingTop:'20%',
                    textAlign:'center',
                    background: 'rgba(255, 255, 255, 0.4)',
                    zIndex: '9999'
                }}/>
                <div style={{ float: 'right', width: '100%' }}>
                    <div style={{ width: '70px', float: 'right' }}>
                        <select
                            onChange={this.handleChange}
                            defaultValue={this.state.locale}
                        >
                            <option value="en-US">USD</option>
                            <option value="de-DE">EUR</option>
                        </select>
                    </div>
                </div>
                <h3 className="center">Get your favorite pizza!</h3>
                <div className="box">
                    {itemList}
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        items: state.rdcr.items,
        api_token: state.ardcr.token,
        activeCartId: state.rdcr.activeCartId,
        cartInitialized: state.rdcr.cartInitialized,
        loaderLoading: state.rdcr.loaderLoading,
        numOfAsync:state.rdcr.numOfAsync
    }
}
const mapDispatchToProps = (dispatch) => {

    return {
        addToCart: (obj) => dispatch(actions.addToCart(obj)),
        onDataGet: (pp, cp) => dispatch(actions.onDataGet(pp, cp)),
        onCartDataGet: (token, activeCartId) => dispatch(actions.onCartDataGet(token, activeCartId)),
        onOrdersGet: (token, identifier) => dispatch(actions.onOrdersGet(token, identifier)),
        initializeCart: () => dispatch({ type: actionTypes.INITIALIZE_CART }),
        initializeLoader: () => dispatch({ type: actionTypes.INITIALIZE_LOADER }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)