import React from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import 'materialize-css';
const Navbar = (props) => {
    return (
        <React.Fragment>
            <nav className="nav-wrapper">
                <div className="container">
                    <Link to="/" className="brand-logo">Yummi Pizza</Link>
                    <ul className="right">
                        <li><Link to="/">Shop</Link></li>
                        <li><Link to="/cart"><i className="material-icons cart-ico-fix">shopping_cart</i></Link></li>
                        <li></li>
                        <li><Link to={(props.api_token)? '/logout':'/login'}><i className="material-icons cart-ico-fix">{(props.api_token)? 'lock_open':'lock_outline'}</i></Link></li>
                    </ul>
                </div>
            </nav>
        </React.Fragment>
    )
}
const mapStateToProps = (state) => {
    return {
        api_token: state.ardcr.token
    }
}
export default connect(mapStateToProps)(Navbar)