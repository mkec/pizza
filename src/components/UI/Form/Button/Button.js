import React from 'react';

import classes from './Button.module.css';

/**
 * @description Button component
 * @summary Three types of buttons: Standard button, Two inline buttons and Ico button
 * @param {object} props 
 */
const button = (props) => {
    let disabledBtn = {
        disabled: ((props.disabled) ? 'disabled' : null)
    }

    let btn =
        <div className="align-right">
            <input className={props.className? props.className : classes.Btn} onClick={props.clicked} type="button" value={props.children} {...disabledBtn} />
        </div>;

    if (props.btnType === 'update' || props.btnType === 'new') {
        btn = <div className={props.className? props.className : classes.BtnMulti + " align-right"}>
            <input className={props.className? props.className : classes.BtnUpd} onClick={props.clicked} type="button" value={props.children} {...disabledBtn} />
        </div>;
    }

    if (props.btnType === 'delete') {
        btn = <div className={props.className? props.className : classes.BtnMulti + " align-right"}>
            <input className={props.className? props.className : classes.BtnDel} onClick={props.clicked} type="button" value={props.children} {...disabledBtn} />
        </div>;
    }
    
    return (
        btn
    );
};

export default button;