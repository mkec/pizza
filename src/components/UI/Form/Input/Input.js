import React from 'react';

import classes from './Input.module.css';
/**
 * @description Input wrapper component
 * @summary Label and input into one component
 * @param {object} props 
 */

const input = (props) => {
    const inputClasses = [classes.Field];
    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(classes.Invalid);
    }
    const classWrapperInput = [classes.FieldWrap];
    if (props.isDatePicker) {
        classWrapperInput.push(classes.Half);
    }

    return (
        <div className={classWrapperInput.join(' ') + ' ' + props.className} >
            <label className={classes.Label} htmlFor={props.id}>{props.title}</label>
            <input 
                key={props.id} 
                className={inputClasses.join(' ')} 
                value={props.value} 
                onChange={props.changed} 
                onClick={props.clicked} 
                type={props.type!=='password'? 'text' : 'password'} 
                readOnly={props.isDatePicker}
                id={props.id} 
                placeholder={props.placeholder} 
            />
        </div>
    );
};

export default input;