import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actionTypes from '../../store/actions/actionTypes';
import classes from './Auth.module.css';

class Logout extends Component {
    constructor(props) {
        super(props)
        this.props.onLogout();
    }
    render() {
        return (
            <div className={classes.Auth} >
                Good bye!
            </div>);
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch({ type: actionTypes.AUTH_LOGOUT }),
    };
}
export default connect(null, mapDispatchToProps)(Logout);