export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const AUTH_LOGOUT_CLEAR = 'AUTH_LOGOUT_CLEAR';
export const AUTH_SESSION_FAIL = 'AUTH_SESSION_FAIL';
export const GET_PIZZAS = 'GET_PIZZAS';
export const GET_CART = 'GET_CART';
export const REMOVE_CART = 'REMOVE_CART';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
export const INPUT_CHANGE = 'INPUT_CHANGE';
export const STORE_ORDER = 'STORE_ORDER';
export const GET_ORDERS = 'GET_ORDERS';
export const INITIALIZE_CART = 'INITIALIZE_CART';
export const INITIALIZE_LOADER = 'INITIALIZE_LOADER';

