import axios from 'axios';
import * as actionType from './actionTypes';

/**
 * @description Handling assync REDUX for adding item to cart
 * @param {*} value 
 */
export const storeOnServer = (value, pizza) => {
    return {
        type: actionType.ADD_TO_CART,
        data: value,
        pizza:pizza
    };
}

/**
 * @description Ajax promise - axios REST call for adding item to cart
 * @param {*} addToCart 
 */
export const addToCart = (sendData) => {
    const config = getConfig(sendData.api_token);
    delete sendData['api_token'];
    Object.keys(sendData).forEach((key) => (sendData[key] === '') && delete sendData[key]);
    return dispach => {
        axios.post(process.env.REACT_APP_HOST + '/api/cart', sendData, config)
            .then(response => {
                dispach(storeOnServer(response.data, sendData));
                localStorage.setItem('activeCartId', response.data.cart_id);
            })
            .catch(error => {
                console.log(error);
                dispach(errorShow(error.response));
                if (error.response.status === 401) {
                    
                }
            });
    }
};

/**
 * @description Handling assync REDUX for adding item to cart
 * @param {*} value 
 */
export const storedOrder = value => {
    return {
        type: actionType.STORE_ORDER,
        data: value
    };
}

/**
 * @description Ajax promise - axios REST call for adding item to cart
 * @param {*} onSubmitOrder 
 */
export const onSubmitOrder = (sendData) => {
    const config = getConfig(sendData.api_token);
    delete sendData['api_token'];
    Object.keys(sendData).forEach((key) => (sendData[key] === '') && delete sendData[key]);
    return dispach => {
        axios.post(process.env.REACT_APP_HOST + '/api/orders', sendData, config)
            .then(response => {
                localStorage.removeItem('activeCartId');
                dispach(storedOrder(response.data));
            })
            .catch(error => {
                console.log(error);
                dispach(errorShow(error.response));
                if (error.response.status === 401) {
                    
                }
            });
    }
};

/**
 * @description Handling assync REDUX for rejecting cart
 * @param {*} value 
 */
export const removeCartItem = (value,id) => {
    return {
        type: actionType.REMOVE_ITEM,
        data: value,
        id:id
    };
}
/* 
export const rejectCart = (token) => {
    const config = getConfig(token);
    return dispach => {
        axios.delete(process.env.REACT_APP_HOST + '/api/cart', config)
            .then(response => {
                dispach(removeCat(response.data));
            })
            .catch(error => {
                console.log(error.response);
                dispach(errorShow(error.response));
                if (error.response.status === 401) {
                    
                }
            });
    }
}; */
/**
 * @description Ajax promise - axios REST call for rejecting cart
 * @param {*} removeItem
 */
export const removeItem = (id,token,cart_id) => {
    const config = (token == null)? getNonAuthConfig() : getConfig(token);
    return dispach => {
        axios.delete(process.env.REACT_APP_HOST + '/api/cart/' + cart_id + '/pizza/' + id, config)
            .then(response => {
                dispach(removeCartItem(response, id));
            })
            .catch(error => {
                console.log(error.response);
                dispach(errorShow(error.response));
                if (error.response.status === 401) {
                    
                }
            });
    }
};

/**
 * @description Handling assync REDUX
 * @param {*} value 
 */
export const errorShow = value => {
    return {
        type: actionType.AUTH_SESSION_FAIL,
        data: value
    };
}

const getConfig = (token) => {
    const config = {
        headers: {
            "Authorization": token,
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    };
    return config;
}

const getNonAuthConfig = () => {
    const config = {
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    };
    return config;
}

/**
 * @description Handling assync REDUX for reading data
 * @param {*} value 
 */
export const getFromServer = value => {
    return {
        type: actionType.GET_PIZZAS,
        data: value
    };
}
/**
 * @description Ajax promise - axios REST call for reading data
 * @param {*} sendData 
 */
export const onDataGet = (perPage, currentPage) => {
    const url = process.env.REACT_APP_HOST + `/api/pizzas?per_page=${perPage}&page=${currentPage}`;
    return dispach => {
        axios.get(url)
            .then(response => {
                dispach(getFromServer(response.data));
            })
            .catch(error => {
                dispach(errorShow(error.response));
                console.log(error);
            });
    }
}


/**
 * @description Handling assync REDUX for reading data
 * @param {*} value 
 */
export const getOrdersFromServer = value => {
    return {
        type: actionType.GET_ORDERS,
        data: value
    };
}

/**
 * @description Ajax promise - axios REST call for reading data
 * @param {*} sendData 
 */
export const onOrdersGet = (token, identifier) => {
    const url = token == null? process.env.REACT_APP_HOST + `/api/orders?identifier=${identifier}` : process.env.REACT_APP_HOST + `/api/orders?token=${token}`;
    return dispach => {
        axios.get(url)
            .then(response => {
                dispach(getOrdersFromServer(response.data));
            })
            .catch(error => {
                dispach(errorShow(error.response));
                console.log(error);
            });
    }
}

/**
 * @description Handling assync REDUX for reading data
 * @param {*} value 
 */
export const getCartFromServer = value => {
    return {
        type: actionType.GET_CART,
        data: value
    };
}
/**
 * @description Ajax promise - axios REST call for get cart or create cart
 * @param {*} api_token 
 */
export const onCartDataGet = (api_token, cart_id) => {
    cart_id = parseInt(localStorage.getItem('activeCartId'));
    const config = (api_token == null)? getNonAuthConfig() : getConfig(api_token);
    const url = process.env.REACT_APP_HOST + '/api/cart/' + cart_id;
    return dispach => {
        axios.get(url, config)
            .then(response => {
                dispach(getCartFromServer(response.data));
            })
            .catch(error => {
                console.log(error);
                if (error.response.status === 400) {
                    console.log('maybe we should create cart');
                    dispach(errorShow(error.response));
                    console.log(error.response);
                } else {
                    dispach(errorShow(error.response));
                    console.log(error.response);
                }
            });
    }
}
