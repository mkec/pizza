import axios from 'axios';

import * as actionTypes from './actionTypes';

/* To Reducer - Per Action */
export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (token, userId, email) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token,
        userId: userId,
        email: email
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};
/**
 * @description Handling Login / SignUp process
 * @summary Send request data for Login/SignUp on server
 *
 * @param {*} email
 * @param {*} password
 * @param {*} isSignup
 * @param {*} name
 * @param {*} password_confirmation
 * @returns
 */
export const auth = (email, password) => {
    const headers = {
        "Accept": "application/json",
        "Content-Type": "application/json"
    }
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password
        };
        let url = process.env.REACT_APP_HOST + '/api/login';//login
        
        axios.post(url, authData, headers)
            .then(response => {
                dispatch(authSuccess(response.data.data.api_token, response.data.data.name, response.data.data.email));
            })
            .catch(err => {
                console.log(err);
                dispatch(authFail(err.response));
            });
    };
};