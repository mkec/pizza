import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';
const initialState = {
    items: [],
    cart: [],
    orders: [],
    currPage: null,
    lastPage: null,
    selectedItem: null,
    paginationButtons: 5,
    totalItemsCount: null,
    perPage: 10,
    activeCartId: localStorage.getItem('activeCartId') ? localStorage.getItem('activeCartId') : null,
    ordered: false,
    cartInitialized: false,
    loaderLoading: false,
    numOfAsync:0,
    deliveryForm: localStorage.getItem('deliveryAddress') ? JSON.parse(localStorage.getItem('deliveryAddress')) : {
        name: {
            elementConfig: {
                type: 'input',
                title: 'First name:',
                placeholder: 'Please enter your first name here...'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        lastName: {
            elementConfig: {
                type: 'input',
                title: 'Last name:',
                placeholder: 'Please enter your last name here...'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        address: {
            elementConfig: {
                type: 'input',
                title: 'Delivery address:',
                placeholder: 'Please enter your address...'
            },
            value: '',
            validation: {
                required: false
            },
            valid: false,
            touched: false
        },
        city: {
            elementConfig: {
                type: 'input',
                title: 'City:',
                placeholder: 'Please enter your city...'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
        zip: {
            elementConfig: {
                type: 'input',
                title: 'ZIP:',
                placeholder: 'Please enter your ZIP code...'
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        },
    },
    formIsValid: localStorage.getItem('validated') ? localStorage.getItem('validated') : false
}

/**
 * @description Checking input field validation
 * @param {string} value 
 * @param {*} rules 
 */
const checkValidity = (value, rules) => {
    let isValid = true;
    if (!rules) {
        return true;
    }

    if (rules.required) {
        isValid = value.trim() !== '' && isValid;
    }

    if (rules.maxVal) {
        isValid = ((value.length <= rules.maxVal) && isValid) ? true : false;
    }

    if (rules.minVal) {
        isValid = ((value.length >= rules.minVal) && isValid) ? true : false;
    }

    return isValid;
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_ORDER:
            return {
                ...state,
                cart: [],
                ordered: true,
                activeCartId: null,
                cartInitialized: false,
                numOfAsync: state.numOfAsync -1,
                loaderLoading: false
            }
        case actionTypes.INITIALIZE_CART:
            console.log('Initialized cart');
            return {
                ...state,
                cartInitialized: true
            }
        case actionTypes.INITIALIZE_LOADER:
            console.log('Initialized loader');
            return {
                ...state,
                numOfAsync: state.numOfAsync +1,
                loaderLoading: true
            }
        case actionTypes.GET_ORDERS:
            let orders = action.data.data;
            return {
                ...state,
                orders,
                ordered: false
            }
        case actionTypes.INPUT_CHANGE:

            let value = action.value;
            const updatedDeliveryForm = {
                ...state.deliveryForm
            };
            const updatedFormElement = {
                ...updatedDeliveryForm[action.formId]
            };
            updatedFormElement.value = value;
            updatedFormElement.valid = checkValidity(updatedFormElement.value, updatedFormElement.validation);
            updatedFormElement.touched = true;
            updatedDeliveryForm[action.formId] = updatedFormElement;

            let formIsValid = true;
            for (let inputIdentifier in updatedDeliveryForm) {
                formIsValid = updatedDeliveryForm[inputIdentifier].valid && formIsValid;
                localStorage.setItem('validated', formIsValid);
            }
            //OR ON ORDER SUBMIT
            localStorage.setItem('deliveryAddress', JSON.stringify(updatedDeliveryForm));
            return {
                ...state,
                deliveryForm: updatedDeliveryForm,
                formIsValid: formIsValid
            }
        case actionTypes.GET_PIZZAS:
            let products = action.data.data;
            let meta = action.data.meta;
            return {
                ...state,
                items: products,
                lastPage: meta.last_page,
                currPage: meta.current_page,
                perPage: meta.per_page,
                totalItemsCount: meta.total,
                numOfAsync: state.numOfAsync -1,
                loaderLoading: false
            }
        case actionTypes.GET_CART:
            let mapped = action.data.data;
            let tmpMap = [];
            if (typeof mapped !== 'undefined') {
                mapped.forEach((item, i) => {
                    tmpMap[i] = item;
                    tmpMap[i]['quantity'] = item.pizza_carts.quantity;
                    tmpMap[i]['cart_id'] = item.pizza_carts.cart_id;
                    tmpMap[i]['pizza_id'] = item.pizza_carts.pizza_id;
                    delete tmpMap[i]['pizza_carts'];
                });
            }
            return {
                ...state,
                cart: tmpMap,
                numOfAsync: state.numOfAsync -1,
                loaderLoading: false
            }
        case actionTypes.ADD_TO_CART:
            console.log('Add to cart');
            let index = 0;
            let items = [];
            let item = {};
            if (state.cart.length > 0 && state.cart.findIndex(item => item.id === action.data.id) > -1) {
                index = state.cart.findIndex(item => item.id === action.data.id);
                items = [
                    ...state.cart
                ]
                item = {
                    ...items[index]
                }
                item.quantity = action.data.quantity;
                items[index] = item;
            } else {
                if (state.cart.length > 0)
                    items = [
                        ...state.cart
                    ]
                let pizza = action.pizza;
                pizza['id'] = action.data.id;
                pizza['cart_id'] = action.data.cart_id;
                pizza['quantity'] = action.data.quantity;
                items.push(pizza);
            }
            return {
                ...state,
                cart: items,
                activeCartId: action.data.cart_id,
                numOfAsync: state.numOfAsync -1,
                loaderLoading: false
            }
        case actionTypes.REMOVE_FROM_CART:
            return {
                ...state,
                cart: [
                    ...state.cart,
                    action.data
                ]
            }
        case actionTypes.REMOVE_CART:
            return {
                ...state,
                cart: []
            }
        case actionTypes.REMOVE_ITEM:
            if (action.data.data.status !== "204") return { state }
            let newArr = state.cart.filter(item => item.id !== action.id);
            return {
                ...state,
                cart: newArr
            }
        default:
    }
    return state
}

export default reducer;